#!/usr/bin/env python
# @author: jafamo@gmail.com
# 

import os
import sys

def docker6():
    pathDspace = raw_input(
        "Select directory to install DSpace, ex: (/home/user/directory) ")
    os.chdir(pathDspace)
    os.system('git clone https://github.com/DSpace-Labs/DSpace-Docker-Images.git')
    print "Downloaded project in: ", pathDspace
    os system('cd DSpace-Docker-Images/docker-compose-files/dspace-compose')
    os.system('docker-compose -p d6 -f docker-compose.yml -f d6.override.yml up -d')
    print "You can visit: http://localhost:8080/xmlui/"
    print "SOLR http://localhost:8080/solr/"
    sys.exit()


def docker7():
    pathDspace = raw_input(
        "Select directory to install DSpace, ex: (/home/user/directory) ")
    os.chdir(pathDspace)
    os.system('git clone https://github.com/DSpace-Labs/DSpace-Docker-Images.git')
    print "Downloaded project in: ", pathDspace
    os system('cd DSpace-Docker-Images/docker-compose-files/dspace-compose')
    os.system(
        'docker-compose -p d7 -f docker-compose.yml -f d7.override.yml up -d')
    print "Image name: dspace/dspace-angular"
    print "You can visit: http://localhost:8080/spring-rest"
    print "DSpace FRONT END Angular: http://localhost:3000"
    sys.exit()


def stopDocker6():
    os.system('docker-compose -p d6 -f docker-compose.yml -f d6.override.yml down')
    os.system('docker ps ')


def stopDocker7():
    os.system('docker-compose -p d7 -f docker-compose.yml -f d7.override.yml down')
    os.system('docker ps ')


def listCommand():
    print "LIST"
    print "INDEX MANUAL DSPACE: docker exec dspace //dspace/bin/dspace index-discovery"


def print_menu():  # Your menu design here
    print 30 * "-", "MENU", 30 * "-"
    print "1. Download and running Dspace 6 "
    print "2. Stop Dspace 6"
    print "3. Download and running Dspace 7 "
    print "5. Exit"
    print 67 * "-"


loop = True

while loop:  # While loop which will keep going until loop = False
    print_menu()  # Displays menu
    choice = input("Enter your choice [1-5]: ")
    if choice == 1:
        print "Menu 1 has been selected"
        docker6()
        # You can add your code or functions here
    elif choice == 2:
        print "Menu 2 has been selected"
        stopDocker()
        # You can add your code or functions here
    elif choice == 3:
        print "Menu 3 has been selected"
        docker7()
        # You can add your code or functions here
    elif choice == 5:
        print "Menu 5 has been selected"
        # You can add your code or functions here
        loop = False  # This will make the while loop to end as not value of loop is set to False
    else:
        # Any integer inputs other than values 1-5 we print an error message
        raw_input("Wrong option selection. Enter any key to try again..")
