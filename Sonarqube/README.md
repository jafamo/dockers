# Sonarqube

Analyse Code Quality with SonarQube and Docker Locally


Start postgres, sonarqube and adminer

```
docker-compose up -d
```


If your sonaqube instance is not working, execute this:

```
docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube
```
